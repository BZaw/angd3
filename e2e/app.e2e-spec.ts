import { AngD3Page } from './app.po';

describe('ang-d3 App', () => {
  let page: AngD3Page;

  beforeEach(() => {
    page = new AngD3Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
