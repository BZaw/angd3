export class Company {
    symbol: string;
    name: string;
    lastSale: string;
    marketCap: string;
    year: string;
    sector: string;
    industry: string;
    summary: string;
    loading: boolean; //shouldn't supposed to be here... interface maybe?
}

//"Symbol", "Name",                                     "LastSale", "MarketCap",    "IPOyear",  "Sector",   "industry",                     "Summary Quote",
//"PIH",    "1347 Property Insurance Holdings, Inc.",   "7.75",     "$46.18M",      "n/a",      "Finance",  "Property-Casualty Insurers",   "http://www.nasdaq.com/symbol/pih",