﻿import { Component, OnInit, ViewChild, ElementRef, Input, OnChanges } from '@angular/core';
import * as d3 from 'd3';
import { Stock } from "app/stock";
import { DSVRowString } from "@types/d3-dsv";
import { Company } from "app/company";
import { DatePipe } from "@angular/common";

@Component({
  selector: 'app-chart-component',
  templateUrl: './chart-component.component.html',
  styleUrls: ['./chart-component.component.css']
})
export class ChartComponentComponent implements OnInit {
  maxY: number;
  @ViewChild('chart') private chartContainer: ElementRef;
  @Input() private data: Array<Array<Stock>>;
  @Input() private companyData: Array<Company>;
  private margin: any = { top: 20, bottom: 20, left: 30, right: 20};
  private chart: any;
  private width: number;
  private height: number;
  private xScale: any;
  private yScale: any;
  private xAxis: any;
  private yAxis: any;
  private element: any;
  
  constructor() { }

  ngOnInit() {
    if (this.data.length > 0) {
      this.maxY = d3.max(this.data, function(array) { return d3.max(array, function (d: Stock) { return Math.max(d.open); });});
      this.renderChart(this.data, this.companyData);
    }
  }

  refresh() {
    this.renderChart(this.data, this.companyData);
  }

  renderChart(data, companyData: Array<Company>) {

    var datepipe = new DatePipe('en-US');

    let element = this.chartContainer.nativeElement;
    this.width = element.offsetWidth - this.margin.left - this.margin.right;
    this.height = element.offsetHeight - this.margin.top - this.margin.bottom;

    // set the ranges
    var x = d3.scaleTime().range([0, this.width]);
    var y = d3.scaleLinear().range([this.height, 0]);

    // define the 1st line
    var valueline = d3.line()
        .x(function(d) { return x(d['date']); })
        .y(function(d) { return y(d['open']); });

    // append the svg obgect to the body of the page
    // appends a 'group' element to 'svg'
    // moves the 'group' element to the top left margin
    var svg = d3.select(element).append('svg')
        .attr("width", this.width + this.margin.left + this.margin.right)
        .attr("height", this.height + this.margin.top + this.margin.bottom)
        .append("g")
        .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

    // Scale the range of the data
    x.domain(d3.extent(data[0], function(d: Stock) { return d.date; }));
    y.domain([0, this.maxY]);

    var h=this.height;

    // add the tooltip area to the webpage
    var tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    var getValue = function(d, key) { return d[key];};

    data.forEach(function(singleDataSet, index){
      var indexClass = index + 1;
      // Add the valueline path.
      svg.append("path")
          .data([singleDataSet])
          .attr("class", "line"+indexClass)
          .attr("d", valueline);

      svg.selectAll(".dot"+indexClass)
          .data(singleDataSet)
          .enter().append("rect")
          .attr("class", "dot"+indexClass).attr("width", 5)
          .attr("height", 5)
          .attr("x", function(d) { return x(d['date']) - 2.5; })
          .attr("y", function(d) {  return y(d['open']) - 2.5; })
          .on("mouseover", function(d) {
          tooltip.transition()
               .duration(200)
               .style("opacity", .9);
          tooltip.html( companyData[index].symbol + ": "
                        + datepipe.transform(getValue(d,"date"),"yyyy-MM-dd")
                        +"<br>Open: " + getValue(d,"open")
                        + "<br> Close: " + getValue(d,"close")
                        + "<br> High: " + getValue(d,"high")
                        + "<br> Low: " +  getValue(d,"low")
                        + "<br>Volume: " + getValue(d,"volume"))
               .style("left", (d3.event.pageX + 5) + "px")
               .style("top", (d3.event.pageY - 128) + "px");
      })
      .on("mouseout", function(d) {
          tooltip.transition()
               .duration(500)
               .style("opacity", 0);
      });
    });
    

    // Add the X Axis
    svg.append("g")
        .attr("transform", "translate(0," + this.height + ")")
        .call(d3.axisBottom(x));

    // Add the Y Axis
    svg.append("g")
        .call(d3.axisLeft(y));

    this.chart = svg;
  }
}
