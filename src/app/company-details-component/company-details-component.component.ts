import { Component, OnInit, Input } from '@angular/core';
import { Company } from "app/company";

@Component({
  selector: 'app-company-details-component',
  templateUrl: './company-details-component.component.html',
  styleUrls: ['./company-details-component.component.css']
})
export class CompanyDetailsComponentComponent implements OnInit {
@Input() private data: Company;
  constructor() { }

  ngOnInit() {
  }

}
