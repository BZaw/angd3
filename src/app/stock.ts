export class Stock {
    //Date,Open,High,Low,Close,Volume,AdjClose
    date: Date;
    open: number;
    high: number;
    low: number;
    close: number;
    volume: number;
    adjClose: number;
}