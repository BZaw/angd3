﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ChartComponentComponent } from './chart-component/chart-component.component';
import { Component } from '@angular/core';
import { DataTableComponentComponent } from './data-table-component/data-table-component.component';
import { CompanyDetailsComponentComponent } from './company-details-component/company-details-component.component';
import { HeaderComponentComponent } from './header-component/header-component.component';
import { DatePipe } from "@angular/common";

@NgModule({
  declarations: [
    AppComponent,
    ChartComponentComponent,
    DataTableComponentComponent,
    CompanyDetailsComponentComponent,
    HeaderComponentComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})

export class AppModule { }
