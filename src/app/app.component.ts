﻿import { Component, OnInit } from '@angular/core';
import { Stock } from "app/stock";
import { Company } from "app/company";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  selectedCompany: Company;
  companyData: Array<Company>;
  selectedStock: Array<Stock>;
  stockData: Array<Array<Stock>>;

    constructor () {}

    title = 'angD3';

    ngOnInit(): void {
      this.stockData = [];
    }
}