import { Component, OnInit, Input } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import * as d3 from 'd3';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { DSVRowString } from "@types/d3-dsv";
import { Stock } from "app/stock";
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { Company } from "app/company";
import { AppComponent } from "app/app.component";

@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css']
})
export class HeaderComponentComponent implements OnInit {
  apiURL: any = 'https://crossorigin.me/http://ichart.finance.yahoo.com/table.csv';
  symbolsURL: any = 'https://crossorigin.me/http://www.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=nasdaq&render=download';
  private selectedStock: Array<Stock>;
  private selectedCompany: Company;
  private stockData: Array<Array<Stock>>;
  public fromDate: NgbDateStruct;
  public toDate: NgbDateStruct;
  public symbolToAdd: Company;
  public selected: Array<Company>;
  private chartData: Array<any>;
  private tempStockData: Array<Array<Stock>>;
  private csvData: string;
  public selectedCompanyText: string;
  public companyData: Array<Company>;
  
  constructor(private appComponent: AppComponent) { }

  ngOnInit() {
    this.selectedCompanyText = '';
    this.selected = [];
    this.stockData = [];
    this.tempStockData = [];
    this.toDate = {year: 2017, month: 1, day: 1};
    this.fromDate = {year: 2016, month: 11, day: 1};
    this.companyData = [];
    this.getSymbols();
  }

  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 1 ? [] //at least one char
        : this.companyData.filter(v => new RegExp(term, 'gi').test(v.name)).splice(0, 10)); //limit to 10

    formatter = (x: {name: string, symbol: string}) => x.name + " (" + x.symbol + ")";

    onSelect($event, input) {
      this.symbolToAdd = $event.item;
    }

    addToList(){
      this.symbolToAdd.loading = true;
      this.selected.push(this.symbolToAdd);
      this.getDataForSymbols(this.symbolToAdd);
      this.symbolToAdd = null;
      this.selectedCompanyText = '';
    }

    deleteFromList(i: number){
      this.selected.splice(i, 1);
    }

    selectStock(i: number){
      this.selectedCompany = this.selected[i];
      this.appComponent.selectedCompany = this.selectedCompany;
      this.selectedStock = this.tempStockData[i];
      this.appComponent.selectedStock = this.selectedStock;
    }

    displayChart() {
      this.stockData = this.tempStockData;
      this.appComponent.stockData = this.stockData;
      this.appComponent.companyData = this.selected;
    }

    getSymbols() {
      var vm = this;
      d3.csv(this.symbolsURL, function(error, data) {
        if (error) throw error;

        var parsedData = [];

        data.forEach(function(d) {
          let rr: DSVRowString = d;
          let pr: Company;

          pr = {
            symbol: rr['Symbol'],
            name: rr['Name'],
            lastSale: rr['LastSale'],
            marketCap: rr['MarketCap'],
            year: rr['IPOyear'],
            sector: rr['Sector'],
            industry: rr['industry'],
            summary: rr['Summary Quote'],
            loading: false
          };
          vm.companyData.push(pr);
        });
      });
    }

    getDataForSymbols(company) {
      var vm = this;
      var parseTime = d3.timeParse("%Y-%m-%d");

      // better that way...
      // yahoo api numerates months from 0 to 11
      let a = vm.fromDate.month - 1;
      let b = vm.fromDate.day;
      let c = vm.fromDate.year;
      let d = vm.toDate.month - 1;
      let e = vm.toDate.day;
      let f = vm.toDate.year;

      d3.csv(`${this.apiURL}?s=${company.symbol}&d=${d}&e=${e}&f=${f}&g=d&a=${a}&b=${b}&c=${c}&ignore=.csv`, function(error, data) {
        if (error) throw error;

        var parsedData = [];

        data.forEach(function(d) {
          let rr: DSVRowString = d;
          let pr: Stock;

          pr = {
            date: parseTime(rr['Date']),
            open: +rr['Open'],
            high: +rr['High'],
            low: +rr['Low'],
            close: +rr['Close'],
            volume: +rr['Volume'],
            adjClose: +rr['Adj Close']
          };

          parsedData.push(pr);
        });
        vm.tempStockData.push(parsedData);
        company.loading = false;
      });
    }
}
