import { Component, OnInit, Input } from '@angular/core';
import { Stock } from "app/stock";

@Component({
  selector: 'app-data-table-component',
  templateUrl: './data-table-component.component.html',
  styleUrls: ['./data-table-component.component.css']
})
export class DataTableComponentComponent implements OnInit {
  @Input() private data: Array<Array<Stock>>;
  constructor() { }

  ngOnInit() {
  }

}
